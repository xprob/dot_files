# Homebrew
eval "$(/opt/homebrew/bin/brew shellenv)"

# Java for scala and hadoops
#export JAVA_HOME=$(/usr/libexec/java_home -version 11)
export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)

# Some hadoop variables
export HADOOP_HOME=/opt/homebrew/Cellar/hadoop/3.3.4/libexec
export SPARK_DIST_CLASSPATH=$(hadoop classpath)
#export HIVE_CONF_DIR=/opt/mambaforge/envs/spark/lib/python3.10/site-packages/pyspark/conf
#export SPARK_HOME=/opt/mambaforge/envs/spark/lib/python3.10/site-packages/pyspark
export SPARK_HOME=/opt/spark33_212
export HIVE_CONF_DIR=/opt/spark33_212/conf

export PYSPARK_PYTHON=/opt/mambaforge/envs/spark/bin/python
export PYSPARK_DRIVER_PYTHON=/opt/mambaforge/envs/spark/bin/python

export PG_TASKAL_USER="task_access"
export PG_TASKAL_PASS="abcd"
