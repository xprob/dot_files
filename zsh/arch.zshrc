# Basic auto/tab complete:
zstyle :compinstall filename '/home/barc/.zshrc'
autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
# Include hidden files.
_comp_options+=(globdots)               

# History
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000

# Prompt
PROMPT="%n@%m %2~ %# "

# Turn of fucking beeps!
unsetopt BEEP
setopt notify

# ls colors
export CLICOLOR=1
export LSCOLORS=ExGxBxDxCxEgEdxbxgxcxd
alias ls="ls --color=auto --time-style=long-iso"

# zsh extensions
# syntax highlighting
# https://github.com/zsh-users/zsh-syntax-highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# autosuggestions
# https://github.com/zsh-users/zsh-autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# better vi mode
# https://github.com/jeffreytse/zsh-vi-mode
source /usr/share/zsh/plugins/zsh-vi-mode/zsh-vi-mode.zsh

# VIM part
export EDITOR=nvim
export VISUAL=nvim
alias vim="nvim"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/barc/miniforge3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/barc/miniforge3/etc/profile.d/conda.sh" ]; then
        . "/home/barc/miniforge3/etc/profile.d/conda.sh"
    else
        export PATH="/home/barc/miniforge3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# Print something sexy
neofetch

