# Basic auto/tab complete:
zstyle :compinstall filename '~/.zshrc'
autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit

# Include hidden files.
_comp_options+=(globdots)               

# History
setopt HIST_IGNORE_ALL_DUPS

# Turn of fucking beeps!
unsetopt BEEP
setopt notify

# grep & coreutils without g at the begining
export PATH="$SPARK_HOME/bin:/opt/homebrew/opt/gnu-sed/libexec/gnubin:/opt/homebrew/opt/coreutils/libexec/gnubin:/opt/homebrew/opt/grep/libexec/gnubin:$PATH"

# ls colors
export CLICOLOR=1
export LSCOLORS=ExGxBxDxCxEgEdxbxgxcxd
alias ls='ls --color=auto --time-style=long-iso'

# neovim
export EDITOR="nvim"
export VISUAL="nvim"
alias vim="nvim"

# Print something sexy
neofetch

# powerline10k
source /opt/homebrew/opt/powerlevel10k/powerlevel10k.zsh-theme

if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/mambaforge/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/mambaforge/etc/profile.d/conda.sh" ]; then
        . "/opt/mambaforge/etc/profile.d/conda.sh"
    else
        export PATH="/opt/mambaforge/bin:$PATH"
    fi
fi
unset __conda_setup

conda activate py31
# <<< conda initialize <<<

# zsh extensions
# syntax highlighting
# https://github.com/zsh-users/zsh-syntax-highlighting
source /opt/homebrew/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# export ZSH_HIGHLIGHT_HIGHLIGHTERS_DIR=/opt/homebrew/share/zsh-syntax-highlighting/highlighters

# autosuggestions
# https://github.com/zsh-users/zsh-autosuggestions
source /opt/homebrew/share/zsh-autosuggestions/zsh-autosuggestions.zsh

# better vi mode
# https://github.com/jeffreytse/zsh-vi-mode
source /opt/homebrew/opt/zsh-vi-mode/share/zsh-vi-mode/zsh-vi-mode.plugin.zsh

# better history managment
# https://github.com/zsh-users/zsh-history-substring-search
# Keys for searching in substring
source /opt/homebrew/share/zsh-history-substring-search/zsh-history-substring-search.zsh
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

# Hadoop control: start & stop
alias hdfs_start='/opt/homebrew/Cellar/hadoop/3.3.4/libexec/sbin/start-all.sh'
alias hdfs_stop='/opt/homebrew/Cellar/hadoop/3.3.4/libexec/sbin/stop-all.sh'

# Spark
#alias start_spark=/opt/homebrew/Cellar/apache-spark/3.2.0/libexec/sbin/start-all.sh
#alias stop_spark=/opt/homebrew/Cellar/apache-spark/3.2.0/libexec/sbin/stop-all.sh
alias pyspark="conda activate spark; ipython --profile=pyspark"

# Hive CLI
alias bee="beeline -u jdbc:hive2://localhost:10000 -n 'hive' -p 'password'"
