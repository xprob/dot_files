--Set Alias
local set = vim.opt

--General
set.compatible = false
set.encoding = 'utf-8'
set.fileformat = 'unix'
set.swapfile = false

--Additional
set.errorbells = false
set.mouse = 'a'
set.termguicolors = true
set.lazyredraw = true

--Tab
set.tabstop = 4 
set.softtabstop = 4
set.shiftwidth = 4
set.expandtab = true

-- Spelling
set.spell = true
set.spelllang = 'en_us'

--Text
set.syntax = 'enable'
set.smartindent = true
set.hidden = true
set.wrap = false
set.scrolloff = 18

--Search
set.incsearch = true
set.hlsearch = true
set.ignorecase = true
set.smartcase = true

--Row Number
set.number = true
set.relativenumber = true

--Packer
require('packer').startup(function()
    use 'wbthomason/packer.nvim'
    use 'Mofiqul/vscode.nvim'
    use 'ethanholz/nvim-lastplace'
    use 'nvim-treesitter/nvim-treesitter'
    use({
        'nvim-lualine/lualine.nvim',
        requires = {
            'kyazdani42/nvim-web-devicons',
            opt = true
        }
    })
end)

--Adjust Colors
vim.g.vscode_style = "dark"
vim.cmd('colorscheme vscode')

--Status Bar
require('lualine').setup {
    options = {
        theme = 'vscode'
    }
    --
--Treesitter
}
require('nvim-treesitter.configs').setup {
  ensure_installed = {'python', 'scala', 'sql', 'bash'},
  auto_install = true,
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = true,
  },
}

--Lastplace
require('nvim-lastplace').setup {
    lastplace_ignore_buftype = {"quickfix", "nofile", "help"},
    lastplace_ignore_filetype = {"gitcommit", "gitrebase", "svn", "hgcommit"},
    lastplace_open_folds = true
}

--Remap Keys
local map = vim.api.nvim_set_keymap
local def_opt = {noremap=true}

--New line in normal mode
map('n', '<C-Return>', 'm`O<Esc>``', def_opt)
map('n', '<Return>', 'm`o<Esc>``', def_opt)

--Copy and paste from clipboard
map('n', '<C-p>', '"*p', def_opt)
map('v', '<C-p>', '"*p', def_opt)
map('v', '<C-y>', '"*y', def_opt)

--Control + h/l to switch tab
map('n', '<C-l>', ':tabn<CR>', def_opt)
map('n', '<C-h>', ':tabp<CR>', def_opt)

--Copy to the end of the line
map('n', 'Y', 'y$', def_opt)

--Control + hjkl in command mode
map('c', '<C-h>', '<Left>', def_opt)
map('c', '<C-j>', '<Down>', def_opt)
map('c', '<C-k>', '<Up>', def_opt)
map('c', '<C-l>', '<Right>', def_opt)

--Keeping it centalized in search
map('n', 'n', 'nzzzv', def_opt)
map('n', 'N', 'Nzzzv', def_opt)
map('n', 'J', 'mzJ`z', def_opt)

--Mark if more than 80 columns
--vim.cmd("call matchadd(\'ColorColumn\', \'\\%81v.\\+\', 100)")
--vim.cmd('hi ColorColumn ctermbg=Grey ctermbg=DarkGrey guibg=Grey guifg=DarkGrey')
